const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcrypt");

const saltRounds = 10;

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "A user must have an email"],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, "A user must have a valid email"],
  },
  password: {
    type: String,
    required: [true, "A user must have a password"],
    minlength: [6, "Password is too short. Minimum of six characters"],
    select: false,
  },
  passwordConfirm: {
    type: String,
    required: [true, "A user must have a passwordConfirm value"],
    validate: {
      validator: function (value) {
        return value === this.password;
      },
      message: "password and passwordConfirm are different",
    },
  },
});

userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();

  this.password = await bcrypt.hash(this.password, saltRounds);
  this.passwordConfirm = undefined;
  next();
});

userSchema.methods.correctPassword = async function (
  currentPassword,
  hashPassword
) {
  return await bcrypt.compare(currentPassword, hashPassword);
};

const User = mongoose.model("User", userSchema);

module.exports = User;
