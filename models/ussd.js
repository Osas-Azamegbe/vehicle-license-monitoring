const mongoose = require("mongoose");

const ussdRequestSchema = new mongoose.Schema({
  plateNumber: {
    type: String,
    required: [true, "A ussd request must have a plate number"],
  },
  roadWorthinessStatus: {
    type: String,
    required: [true, "A ussd request must have a road worthiness status"],
  },
  vehicleLicenseStatus: {
    type: String,
    required: [true, "A ussd request must have a vehicle license status"],
  },
  vehicleInsuranceStatus: {
    type: String,
    required: [true, "A ussd request must have a vehicle insurance status"],
  },
});

const UssdRequest = mongoose.model("UssdRequest", ussdRequestSchema);

module.exports = UssdRequest;
