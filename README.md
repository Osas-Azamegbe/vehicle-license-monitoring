# vehicle-license
API base URL => https://vehicle-monitoring.herokuapp.com/api/v1/  
The USSD callback URL => https://vehicle-monitoring.herokuapp.com/api/v1/ussd  
Swagger documentation link => https://vehicle-monitoring.herokuapp.com/api/v1/doc  

## Authentication

### Signup
To sign up make a POST request to `{api wbe url}/api/v1/auth/signup`. An example payload associated with this is request is
```
{
    "email": "user@gmail.com",
    "password": "password",
    "passwordConfirm": "password"
}
```
all object properties are compulsory. A response example is
```
{
  "status": "success",
  "token": "json web token value",
  "data": { 
    "user": "new user data" 
  },
}
```

### Login

To login make a POST request to `{api wbe url}/api/v1/auth/login`. An example payload associated with this is request is
```
{
    "email": "user@gmail.com",
    "password": "password"
}
```
all object properties are compulsory. A response example is
```
{
  "status": "success",
  "data": { 
    "token": "json web token value"
  },
}
```

## USSD
USSD callback URL => `{api web url}/api/v1/ussd`
The callback URL supports only the POST http method

To get a list of USSD requests, make a GET request to `{api web url}/api/v1/ussd/requests`\
This route is protected so a auth json web token is needed. Put the token in the value for the authorization header.\
The value should be "Bearer {token value}"\
To paginate, set values for the "page" and "limit" query parameters. The default value for "limit" parameter is 5\
A response example is 
```
{
  "status": "success",
  "results": "number of fetched USSD requests",
  "data": {
    "ussdRecords": [
      {
        "plateNumber" : "vehicle plate number",
        "roadWorthinessStatus": "value will be either Expired or Valid",
        vehicleLicenseStatus: "value will be either Expired or Valid",
        vehicleInsuranceStatus: "value will be either Expired or Valid",
      }
    ]
  },
}
```
