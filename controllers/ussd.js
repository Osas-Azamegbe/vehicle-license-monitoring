const UssdRequest = require("../models/ussd");

async function saveUssdRequest(plateNumber, statuses) {
  let data = {
    plateNumber,
    roadWorthinessStatus: statuses[0],
    vehicleLicenseStatus: statuses[1],
    vehicleInsuranceStatus: statuses[2],
  };
  try {
    const newTour = await UssdRequest.create(data);
    console.log("ussd request saved successfully");
  } catch (err) {
    console.log(err);
  }
}

exports.ussdInterface = (req, res, next) => {
  /*
    #swagger.tags = ['USSD']
    #swagger.description = 'USSD callback URL to handle USSD requests'
  */

  let { sessionId, serviceCode, phoneNumber, text } = req.body;

  let response = "";

  if (text === "") {
    response = `CON Welcome to Vehicle Compliance Monitoring Service.
        Enter your vehicle plate number followed by #`;
  } else if (text[text.length - 1] === "#") {
    let statuses = [];
    for (let i = 0; i < 3; i++) {
      statuses[i] = Math.floor(Math.random() * 2) ? "Valid" : "Expired";
    }

    response = `END Service status for ${text.substring(0, text.length - 1)}
        Road Worthiness Status: ${statuses[0]}
        Vehicle License Status: ${statuses[1]}
        Vehicle Insurance Status: ${statuses[2]}`;

    saveUssdRequest(text.substring(0, text.length - 1), statuses);
  }

  return res.set("Content-Type: text/plain").send(response);
};

exports.ussdRecords = async (req, res, next) => {
  /*  
    #swagger.tags = ['USSD']
    #swagger.description = 'Make an http request to this endpoint to fetch saved USSD requests. All requests are returned except when the page query parameter is specified. This route requires auth token to be accessed'
    #swagger.parameters['page'] = { 
      description: 'Query parameter that specifies the page of ussd requests to fetch', 
      type: 'integer'
   }
   #swagger.parameters['limit'] = { 
      description: 'Query parameter that specifies the maximum amount of ussd requests in a page. The default value is 5', 
      type: 'integer'
   }
    #swagger.responses[200] = {
      schema: {$ref : "#/definitions/ussdRecordsResponse"},
      description: 'JSON response sent on successful http request'
    }

  */
  try {
    let query = UssdRequest.find({});

    if (req.query.page) {
      const page = parseInt(req.query.page, 10);
      const limit = parseInt(req.query.limit, 10) || 5;
      const skipRequests = (page - 1) * limit;
      query = query.skip(skipRequests).limit(limit);
    }

    const ussdRecords = await query.exec();

    res.status(200).json({
      status: "success",
      results: ussdRecords.length,
      data: {
        ussdRecords,
      },
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      message: err,
    });
  }
};
