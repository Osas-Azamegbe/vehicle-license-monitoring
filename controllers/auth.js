const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const User = require("../models/users");

async function getToken(id) {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
}

exports.signup = async (req, res, next) => {
  /*  
    #swagger.tags = ['Authentication']
    #swagger.description = 'This is the signup endpoint.'
    #swagger.parameters['obj'] = {
      in: 'body',
      schema: {$ref : "#/definitions/signupInput"},
      description: 'JSON payload for signup route'
    }
    #swagger.responses[201] = {
      schema: {$ref : "#/definitions/authenticatedResponse"},
      description: 'JSON response sent on successful signup'
    }

  */

  try {
    const newUser = await User.create({
      email: req.body.email,
      password: req.body.password,
      passwordConfirm: req.body.passwordConfirm,
    });

    const token = await getToken(newUser._id);

    res.status(201).json({
      status: "success",
      data: { token },
    });
  } catch (err) {
    err.statusCode = 400;
    err.status = "fail";
    next(err);
  }
};

exports.login = async (req, res, next) => {
  /*  
    #swagger.tags = ['Authentication']
    #swagger.description = 'This is the login endpoint.'
    #swagger.parameters['obj'] = {
      in: 'body',
      schema: {$ref : "#/definitions/loginInput"},
      description: 'JSON payload for login route'
    }
    #swagger.responses[200] = {
      schema: {$ref : "#/definitions/authenticatedResponse"},
      description: 'JSON response sent on successful login'
    }

  */

  const { email, password } = req.body;

  if (!email || !password) {
    const err = new Error("please provide email and password");
    err.statusCode = 400;
    err.status = "fail";
    return next(err);
  }

  try {
    const user = await User.findOne({ email }).select("+password");
    if (!user) throw new Error("incorrect email");

    const correct = await user.correctPassword(password, user.password);
    if (!correct) throw new Error("incorrect password");

    const token = await getToken(user._id);

    return res.status(200).json({
      status: "success",
      data: { token },
    });
  } catch (err) {
    err.status = "fail";
    err.statusCode = 400;
    next(err);
  }
};

exports.protect = async (req, res, next) => {
  if (
    !req.headers.authorization ||
    !req.headers.authorization.startsWith("Bearer")
  ) {
    const err = new Error("invalid authorization header passed");
    err.status = "fail";
    err.statusCode = 400;
    return next(err);
  }

  const token = req.headers.authorization.split(" ")[1];
  try {
    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    const user = await User.findById(decoded.id).exec();
    console.log(user);
    if (!user) throw new Error("user does not exist");

    return next();
  } catch (err) {
    err.status = "fail";
    err.statusCode = 401;
    return next(err);
  }
};
