const path = require("path");
require("dotenv").config({ path: path.join(__dirname, "config.env") });
const mongoose = require("mongoose");

mongoose
  .connect(process.env.DB_CLOUD, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("MongoDB database connected successfully");
  })
  .catch(() => {
    console.log("Failed to connect to MongoDB database");
  });

const app = require("./app");

const port = 3000;
app.listen(port, () => {
  console.log(`server started on port ${port}`);
});
