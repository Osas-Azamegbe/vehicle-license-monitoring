const path = require("path");
require("dotenv").config({ path: path.join(__dirname, "config.env") });
const swaggerAutogen = require("swagger-autogen")();

const outputFile = "./swagger_output.json";
const endpointsFiles = ["./route/auth.js", "./route/ussd.js"];

const doc = {
  info: {
    version: "1.0.0",
    title: "Vehicle Compliance Monitoring  API",
    description: "Documentation for vehicle compliance monitoring API.",
  },
  basePath: "/",
  schemes: ["http", "https"],
  consumes: ["application/json"],
  produces: ["application/json"],
  tags: [
    {
      name: "Authentication",
      description: `Endpoints related to token based authentication. The JWT lasts for ${process.env.JWT_EXPIRES_IN}`,
    },
    {
      name: "USSD",
      description: "Endpoints related to USSD requests",
    },
  ],
  definitions: {
    signupInput: {
      $email: "example@gmail.com",
      $password: "password",
      $passwordConfirm: "password",
    },
    loginInput: {
      $email: "example@gmail.com",
      $password: "password",
    },
    authenticatedResponse: {
      status: "success",
      data: { token: "jwt value" },
    },
    ussdRecordsResponse: {
      status: "success",
      results: 1,
      data: {
        ussdRecords: [
          {
            _id: "object id",
            plateNumber: "vehicle plate number",
            roadWorthinessStatus: "Expired or Valid",
            vehicleLicenseStatus: "Expired or Valid",
            vehicleInsuranceStatus: "Expired or Valid",
          },
        ],
      },
    },
  },
};

swaggerAutogen(outputFile, endpointsFiles, doc);
