const router = require("express").Router();
const ussdController = require("../controllers/ussd");
const authController = require("../controllers/auth");

router.post("/", ussdController.ussdInterface);
router.get("/requests", authController.protect, ussdController.ussdRecords);

module.exports = router;
