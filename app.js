const express = require("express");
const morgan = require("morgan");
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger_output.json");
const { globalError } = require("./controllers/error");
const ussdRoute = require("./route/ussd");
const authRoute = require("./route/auth");

const app = express();

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(express.json());

const baseUrl = "/api/v1";

app.use(`${baseUrl}/ussd`, ussdRoute);
app.use(`${baseUrl}/auth`, authRoute);

app.use(`${baseUrl}/doc`, swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.all("*", (req, res, next) => {
  const err = new Error(
    `Can't find ${req.method} ${req.originalUrl} on this server`
  );
  err.status = "fail";
  err.statusCode = 404;
  next(err);
});

app.use(globalError);

module.exports = app;
